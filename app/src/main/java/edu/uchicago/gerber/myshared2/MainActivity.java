package edu.uchicago.gerber.myshared2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonRed, buttonGreen, buttonBlue;

    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        linearLayout = (LinearLayout) findViewById(R.id.activity_main);

        String strColor =   PrefsMgr.getString(this, PrefsMgr.COLOR_PREF, "RED");


        if (savedInstanceState == null ) {
            switch (strColor){

                case "RED":

                    linearLayout.setBackgroundColor(Color.RED );
                    break;

                case "GREEN":

                    linearLayout.setBackgroundColor(Color.GREEN );

                    break;

                case "BLUE":

                    linearLayout.setBackgroundColor(Color.BLUE );

                    break;

                default:

                    break;

            }
        }


        buttonBlue = (Button) findViewById(R.id.buttonBlue);
        buttonGreen = (Button) findViewById(R.id.buttonGreen);
        buttonRed = (Button) findViewById(R.id.buttonRed);

        buttonBlue.setOnClickListener(this);
        buttonGreen.setOnClickListener(this);
        buttonRed.setOnClickListener(this);



    }


    @Override
    public void onClick(View v) {

        Button button = (Button) v;

        switch (button.getText().toString()){

            case "RED":


                PrefsMgr.setString(this, PrefsMgr.COLOR_PREF, "RED" );

                linearLayout.setBackgroundColor(Color.RED );
                break;

            case "GREEN":

                linearLayout.setBackgroundColor(Color.GREEN );
                PrefsMgr.setString(this, PrefsMgr.COLOR_PREF, "GREEN" );
                break;

            case "BLUE":

                linearLayout.setBackgroundColor(Color.BLUE );
                PrefsMgr.setString(this, PrefsMgr.COLOR_PREF, "BLUE" );
                break;

            default:

                break;

        }


    }
}
